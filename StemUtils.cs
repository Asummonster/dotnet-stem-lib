﻿using System;
using System.Collections.Generic;

namespace Stem
{
    internal static class StemUtils
    {
        public enum Package
        {
            Message,
            Subscribe,
            Unsubscribe,
            Ping,
            Pong
        }
        internal static List<byte> BuildPackage(Package type, byte[] channel, byte[] message)
        {
            List<byte> pack = new List<byte> { (byte)type };
            if (type == Package.Ping || type == Package.Pong)
            {
                foreach (byte b in channel)
                {
                    pack.Add(b);
                }
            }
            else
            {
                pack.Add((byte)channel.Length);
                foreach (byte b in channel)
                {
                    pack.Add(b);
                }
                foreach (byte b in message)
                {
                    pack.Add(b);
                }
            }
            UInt16 len = (UInt16)pack.Count;
            pack.Insert(0, (byte)(len >> 8 & 255));
            pack.Insert(1, (byte)(len & 255));
            return pack;
        }
    }
}
