﻿using System;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Threading;

using static Stem.StemUtils;
using static System.Text.Encoding;

namespace Stem
{
    public delegate void MessageHandler(string channel, string message);
    public class StemConnection
    {
        private bool Running { get; set; }
        private List<List<byte>> Queue { get; set; }
        private Dictionary<string, bool> Channels { get; set; }
        private TcpClient Client { get; set; }
        private Thread Thread { get; set; }
        public event MessageHandler OnMessage;
        public event MessageHandler OnPong;

        public void Send(string channel, string message)
        {
            Queue.Add(BuildPackage(Package.Message, UTF8.GetBytes(channel), UTF8.GetBytes(message)));
        }
        public void Subscribe(string channel, bool force = false)
        {
            if (force || !Channels.TryGetValue(channel, out bool value) || !value)
            {
                Channels[channel] = true;
                Queue.Add(BuildPackage(Package.Subscribe, UTF8.GetBytes(channel), new byte[0]));
            }
        }
        public void Unsubscribe(string channel)
        {
            if (Channels.TryGetValue(channel, out bool value) && value)
            {
                Channels[channel] = false;
                Queue.Add(BuildPackage(Package.Unsubscribe, UTF8.GetBytes(channel), new byte[0]));
            }
        }
        private void Pong(byte[] content)
        {
            Queue.Add(BuildPackage(Package.Pong, content, new byte[0]));
        }
        public void Ping(byte[] content)
        {
            Queue.Add(BuildPackage(Package.Pong, content, new byte[0]));
        }
        public void Disconnect()
        {
            Running = false;
            Client.Close();
        }

        private void Incoming(NetworkStream stream)
        {
            byte[] raw = new byte[65536];
            int read = 0;
            while (stream.DataAvailable && read < 65536)
            {
                read += stream.Read(raw, read, 1);
            }
            int processed = 0;
            while (processed < read)
            {
                UInt16 packetsize = (UInt16)(raw[processed] << 8 | raw[processed + 1]);
                byte[] packet = new byte[packetsize];
                for (int i = processed + 2; i < processed + packetsize + 2; i++)
                {
                    packet[i - processed - 2] = raw[i];
                }
                processed += packetsize + 2;
                Package type = (Package)packet[0];
                if (type == Package.Ping)
                {
                    byte[] response = new byte[packetsize - 1];
                    for (int i = 1; i < packetsize; i++)
                    {
                        response[i - 1] = packet[i];
                    }
                    Pong(response);
                }
                else if (type == Package.Pong)
                {
                    string content = UTF8.GetString(packet, 1, packetsize - 1);
                    OnPong?.Invoke("pong", content);
                }
                else if (type == Package.Message)
                {
                    int channelsize = packet[1];
                    string channel = UTF8.GetString(packet, 2, channelsize);
                    string message = UTF8.GetString(packet, 2 + channelsize, packetsize - channelsize - 2);
                    OnMessage?.Invoke(channel, message);
                }
            }
        }
        public StemConnection(string address = "stem.fomalhaut.me", int port = 5733)
        {
            Running = true;
            Client = new TcpClient(address, port);
            Queue = new List<List<byte>>();
            Channels = new Dictionary<string, bool>();
            Thread = new Thread(() =>
            {
                NetworkStream stream = Client.GetStream();
                while (Running)
                {
                    if (!Client.Connected)
                    {
                        Client.Connect(address, port);
                        stream = Client.GetStream();
                        foreach (string key in Channels.Keys)
                        {
                            if (Channels[key])
                            {
                                Subscribe(key, true);
                            }
                        }
                    }
                    if (stream.DataAvailable && stream.CanRead)
                    {
                        Incoming(stream);
                    }
                    if (Queue.Count > 0 && stream.CanWrite)
                    {
                        while (stream != null && Queue.Count > 0 && stream.CanWrite)
                        {
                            List<byte> message = Queue[0];
                            byte[] buff = new byte[1];
                            if (message != null)
                            {
                                while (stream != null && message.Count > 0 && stream.CanWrite)
                                {
                                    buff[0] = message[0];
                                    stream.Write(buff, 0, 1);
                                    message.RemoveAt(0);
                                }
                                if (message.Count == 0)
                                {
                                    Queue.RemoveAt(0);
                                }
                            }
                        }
                        stream.Flush();
                    }
                    Thread.Sleep(10);
                }
            });
            Thread.Start();
        }
        ~StemConnection()
        {
            Disconnect();
        }
    }
}