## STEM C# Client
[Server](https://gitlab.com/UnicornFreedom/stem)

### Usage
Create a connection
```cs
Stem.StemConnection server = new Stem.StemConnection([string address = "stem.fomalhaut.me"][, int port = 5733]);
```
Subscribe to channel
```cs
server.Subscribe("test");
```
Setup a receiver
```cs
server.OnMessage += (string channel, string message) =>
{
    Console.WriteLine($"{channel}: {message}");
};
```
Send message
```cs
server.Send("test", "Hi!");
```

## Example
[WinForms Chat](https://gitlab.com/Asummonster/dotnet-stem-chat/)
